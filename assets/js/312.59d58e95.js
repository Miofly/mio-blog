(window.webpackJsonp=window.webpackJsonp||[]).push([[312],{843:function(t,e,r){"use strict";r.r(e);var a=r(18),s=Object(a.a)({},(function(){var t=this,e=t.$createElement,r=t._self._c||e;return r("ContentSlotsDistributor",{attrs:{"slot-key":t.$parent.slotKey}},[r("h2",{attrs:{id:"📚blog"}},[r("a",{staticClass:"header-anchor",attrs:{href:"#📚blog"}},[t._v("#")]),t._v(" 📚Blog")]),t._v(" "),r("p",[t._v("这是一个兼具博客文章、知识管理、文档查找的个人网站，主要内容是Web前端技术。")]),t._v(" "),r("h2",{attrs:{id:"联系我"}},[r("a",{staticClass:"header-anchor",attrs:{href:"#联系我"}},[t._v("#")]),t._v(" 联系我")]),t._v(" "),r("ul",[r("li",[t._v("邮箱："),r("code",[t._v("953212389@qq.com")])])]),t._v(" "),r("h2",{attrs:{id:"前端学习"}},[r("a",{staticClass:"header-anchor",attrs:{href:"#前端学习"}},[t._v("#")]),t._v(" 前端学习")]),t._v(" "),r("br"),t._v(" "),r("img",{staticStyle:{width:"200px"},attrs:{src:"https://miofly.gitee.io/res/img/me/gzh_new.jpg"}}),t._v(" "),r("p",[t._v("关注公众号，回复 "),r("code",[t._v("js")]),t._v(" 等，即可获取这些学习资源。")]),t._v(" "),r("h2",{attrs:{id:"免责声明"}},[r("a",{staticClass:"header-anchor",attrs:{href:"#免责声明"}},[t._v("#")]),t._v(" 免责声明")]),t._v(" "),r("p",[t._v("此博客包含 "),r("a",{attrs:{href:"https://github.com/ruanyf",target:"_blank",rel:"nofollow noopener noreferrer"}},[t._v("阮一峰"),r("OutboundLink")],1),t._v("， "),r("a",{attrs:{href:"https://weibo.com/liaoxuefeng",target:"_blank",rel:"nofollow noopener noreferrer"}},[t._v("廖雪峰"),r("OutboundLink")],1),t._v(" 以及 "),r("strong",[t._v("你不知道的 javascript")]),t._v(" 的博客或书籍。")]),t._v(" "),r("p",[t._v("在这里给他们致以诚挚的感谢。")]),t._v(" "),r("div",{staticClass:"custom-block tip"},[r("p",{staticClass:"custom-block-title"},[t._v("提示")]),t._v(" "),r("p",[t._v("此博客所有文章以及内容，如有侵权请告知，联系邮箱 "),r("code",[t._v("953212389@qq.com")])])])])}),[],!1,null,null,null);e.default=s.exports}}]);