(window.webpackJsonp=window.webpackJsonp||[]).push([[177],{686:function(e,t,s){"use strict";s.r(t);var i=s(18),a=Object(i.a)({},(function(){var e=this,t=e.$createElement,s=e._self._c||t;return s("ContentSlotsDistributor",{attrs:{"slot-key":e.$parent.slotKey}},[s("Boxx"),e._v(" "),s("p"),s("div",{staticClass:"table-of-contents"},[s("ul",[s("li",[s("a",{attrs:{href:"#webstorm-使用-git-rebase-i-的问题"}},[e._v("webstorm 使用 git rebase -i 的问题")])]),s("li",[s("a",{attrs:{href:"#git-rebase-i-有什么作用"}},[e._v("git rebase -i 有什么作用")])]),s("li",[s("a",{attrs:{href:"#如何合并成一条-commit-记录呢"}},[e._v("如何合并成一条 commit 记录呢")])])])]),s("p"),e._v(" "),s("h2",{attrs:{id:"webstorm-使用-git-rebase-i-的问题"}},[s("a",{staticClass:"header-anchor",attrs:{href:"#webstorm-使用-git-rebase-i-的问题"}},[e._v("#")]),e._v(" webstorm 使用 git rebase -i 的问题")]),e._v(" "),s("p",[e._v("当我在 "),s("code",[e._v("webstorm")]),e._v(" 中执行 "),s("code",[e._v("git rebase -i 53675e5b")]),e._v(" 时，它没有使用 "),s("code",[e._v("vi")]),e._v(" 编辑器，而是使用 "),s("code",[e._v("webstorm")]),e._v("  自带的 "),s("code",[e._v("command line")]),e._v(" 但是不知道为什么总会一闪而过，或者无法编辑。")]),e._v(" "),s("p",[e._v("在 "),s("code",[e._v("webstorm")]),e._v(" 中的配置如下")]),e._v(" "),s("blockquote",[s("p",[e._v('git config core.editor "webstorm -w"')])]),e._v(" "),s("p",[e._v("后续改用 "),s("code",[e._v("mac")]),e._v(" 的编辑文本，命令如下")]),e._v(" "),s("blockquote",[s("p",[e._v('git config core.editor "open -W -n"')])]),e._v(" "),s("h2",{attrs:{id:"git-rebase-i-有什么作用"}},[s("a",{staticClass:"header-anchor",attrs:{href:"#git-rebase-i-有什么作用"}},[e._v("#")]),e._v(" git rebase -i 有什么作用")]),e._v(" "),s("p",[e._v("当你运行 "),s("code",[e._v("git rebase -i hash")]),e._v(" 时，你会进入一个编辑器会话，其中列出了所有正在被变基的提交，以及可以对其执行的操作的多个选项。你可以根据如下选项去操作每一个 "),s("code",[e._v("commit")])]),e._v(" "),s("div",{staticClass:"language- line-numbers-mode"},[s("pre",{pre:!0,attrs:{class:"language-text"}},[s("code",[e._v("p, pick = use commit （使用这个commit）\nr, reword = use commit, but edit the commit message（改变commit 的信息）\ns, squash = use commit, but meld into previous commit（融入先前的commit）\nf, fixup = like “squash”, but discard this commit’s log message(放弃此提交的日志消息)\nd, drop = remove commit(移除commit)\n")])]),e._v(" "),s("div",{staticClass:"line-numbers-wrapper"},[s("span",{staticClass:"line-number"},[e._v("1")]),s("br"),s("span",{staticClass:"line-number"},[e._v("2")]),s("br"),s("span",{staticClass:"line-number"},[e._v("3")]),s("br"),s("span",{staticClass:"line-number"},[e._v("4")]),s("br"),s("span",{staticClass:"line-number"},[e._v("5")]),s("br")])]),s("h2",{attrs:{id:"如何合并成一条-commit-记录呢"}},[s("a",{staticClass:"header-anchor",attrs:{href:"#如何合并成一条-commit-记录呢"}},[e._v("#")]),e._v(" 如何合并成一条 commit 记录呢")]),e._v(" "),s("p",[e._v("当我们要合并两条记录时，将第二条记录的 "),s("code",[e._v("pick")]),e._v(" 改为 "),s("code",[e._v("s")]),e._v(" 即可"),s("br"),e._v(" "),s("img",{attrs:{src:"https://img-blog.csdnimg.cn/202005211433200.png",alt:""}}),s("br"),e._v("\n保存后出现如下界面"),s("br"),e._v(" "),s("img",{attrs:{src:"https://img-blog.csdnimg.cn/2020052114334519.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MDAxMzgxNw==,size_16,color_FFFFFF,t_70",alt:""}}),s("br"),e._v("\n这个界面需要我们处理提交信息的修改，如果只需要一个提交 "),s("code",[e._v("message")]),e._v(" ,可把另一个message前面加上 # 号，也可以不做处理，直接保存，保存后再次查看两个记录就合并成一个记录了。"),s("br"),e._v(" "),s("img",{attrs:{src:"https://img-blog.csdnimg.cn/20200521143451730.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MDAxMzgxNw==,size_16,color_FFFFFF,t_70",alt:""}}),s("br"),e._v("\n可能出现的问题，把第一条记录的 "),s("code",[e._v("pick")]),e._v(" 改为了 "),s("code",[e._v("s")]),s("br"),e._v(" "),s("img",{attrs:{src:"https://img-blog.csdnimg.cn/20200521144127560.png",alt:""}}),s("br"),e._v("\n注意不要合并先前提交的东西，也就是已经提交远程分支的纪录。"),s("br"),e._v("\n可使用其提示的命令 "),s("code",[e._v("git rebase --edit-todo")]),e._v("，重新修改 "),s("code",[e._v("rebase")]),e._v(" 时提示的信息，保存后再执行 "),s("code",[e._v("git rebase --continue")]),e._v(" 修改提交的 "),s("code",[e._v("meseage")]),e._v(" 信息"),s("br"),e._v("\n也可以直接执行 "),s("code",[e._v("git rebase --abort")]),e._v(" （会放弃合并，回到rebase操作之前的状态，之前的提交的不会丢弃），然后重新执行 "),s("code",[e._v("git rebase -i")])])],1)}),[],!1,null,null,null);t.default=a.exports}}]);